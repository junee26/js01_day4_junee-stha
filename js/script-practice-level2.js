//Task 1 - Task 1 - Perform addition, subtraction, multiplication, and division by taking input using the prompt() function and show all the results by concatenating it with a string like: sum is ... , product is ...

let firstNum = prompt("Enter first number");
    firstNum = parseInt(firstNum);
let lastNum = prompt("Enter second number");
    lastNum = parseInt(lastNum);

    
    console.log("Sum is " + (firstNum + lastNum) );
    console.log("Subtract is " + (firstNum - lastNum) );
    console.log("Product is " + (firstNum * lastNum) );
    console.log("Division is " + (firstNum / lastNum) );

// end of Task 1

//Task 2 - Declare 5 different variables using let and 5 different variables using var but don't assign values to it and log all those variables in the console

let var1 
let var2
let var3 
let var4 
let var5
console.log( var1, var2, var3, var4, var5 ); //undefined

var var6 
var var7 
var var8 
var var9 
var var10
console.log( var6, var7, var8, var9, var10 ); //undefined

// end of Task 2

//Task 3 - Create a constant and try to create another constant with the same name and see what happens, fix the mistake if something goes wrong.


const foo = 1;
/** Uncaught SyntaxError: Identifier 'foo' has already been declared if uncomment line 39 */
// const foo = 2;

// end of Task 3

//Task 4: - Declare two variables using let on the same line without terminating the line and store true value in both the variables and perform addition and show the result on the console and try to analyze the code.

let num1, num2;
    num1 = 5; 
    num2 = 12;

    console.log("Sum is " + (num1 + num2) );
    
let num3 = 0, num4 = 10;
    console.log("Sum is " + (num3 + num4) );
// end of Task 4