//Task 1 - Explore parseFloat() function and pass integer like 1 or 2 and show the result of it on the console

let firstNum = parseFloat(1);
let lastNum = parseFloat(10.2);

    console.log("Sum is " + (firstNum + lastNum) );

// end of Task 1

//Task 2 - Use the + unary operator in a string that contains the number '999' and add 1 to it and show the output on the console. Note: the output should be 1000
let number = '999';
    number = parseInt(number);
    number = number + 1;
    console.log(number); // 1000

// end of Task 2

//Task 3 - Use the - unary operator to the string containing a number and show the message into the console

let number1 = '5';
    number1 = -number1;
    console.log("value becomes numeric " + number1);
    

// end of Task 3

//Task 4: - Identify the data type of the value returned by typeof operator (Hint: you can test it on the console)

let x = 1;
let y = 2;
console.log(typeof (x * y));
console.log(typeof "name");

// end of Task 4

//Task 5 - Store string using double-quote syntax and try to show something like Ram said, "Programming is a fun game."

let stringValue = "Programming is a fun game.";
console.log("Ram said, " + '"'+stringValue+'"');

// end of Task 5

//Task 6 - Create an object using object literal syntax and use variable name email for creating an object, create properties like: sendTo and sentFrom properties and give values to it, create send method which will log the message like "message sent."
let email = {
    sendTo  : "Junee",
    sentFrom  : "Sunee",
    send  : function(){
        console.log("message sent");
    }
}
email.send();

