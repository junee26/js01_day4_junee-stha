//Task 1 - declare variables, but assign values to those variables after declaration on the next line and show it on a popup and in a console.

//declare variables
let firstNum,secondNum; 
    //assign values
    firstNum = 11111; 
    secondNum = 22222;

    //popoup
    alert(firstNum);
    alert(secondNum);

    //console
    console.log(firstNum);
    console.log(secondNum);

// end of Task 1

//Task 2 - use a different naming convention and create a few variables of your choice and store values of different data types in it.

//camelCase 
let mobileModel;               //undefined
let waterResistance = true;    //boolean          
let mobileName = "Samsung";    //String
let mobilePrice = 55000;       //Number
let mobileDetails = {          //Object
    mobileMemory : "8GB",
    mobileCamera : "64MP"
};

console.log(typeof mobileModel);
console.log(typeof waterResistance);
console.log(typeof mobileName);
console.log(typeof mobilePrice);
console.log(typeof mobileDetails);

//snakeCase 
let mobile_model;               //undefined
let mobile_name = "Samsung";    //String
let mobile_price = 55000;       //Number
let mobile_details = {          //Object
    mobile_memory : "8GB",
    mobile_camera : "64MP"
};

console.log(typeof mobile_model);
console.log(typeof mobileName);
console.log(typeof mobilePrice);
console.log(typeof mobileDetails);

// end of Task 2

//Task 3 - Take 2 different inputs using prompt() and use string concatenation operator to show the results in the console

let firstName = prompt("Enter first name");
let lastName = prompt("Enter second name");

console.log("Hello " + firstName +" "+ lastName);

// end of Task 3

// Task 4: Show warnings and errors in the console

let color = "blue";

console.warn("This is a warning");
console.error("This is a error");

// end of Task 4

//Task 5: Explore what is dynamically typed language, and why is js a dynamically typed language.

/*does not require the explicit declaration of the variables*/

var num = 1;
console.log(typeof num);
    num = num + 1;
    console.log(num);
    
// end of Task 5