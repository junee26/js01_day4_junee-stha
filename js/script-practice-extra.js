//Task 1 - Create a variable to store the name of the city you live in and test it on the console

let place = "kathmandu"
console.log("I live in " + place );

// end of Task 1

//Task 2 - Create a variable named isProgrammingEngaging and supply any boolean value you prefer to supply.

let isProgrammingEngaging = true;
if(isProgrammingEngaging == true){
    console.log("Yes it is engaging");
}

// end of Task 2

//Task 3 - Let's suppose I want to store my age, what would be the appropriate data type to store age, and initialize that variable with the appropriate value. In the end, don't forget to see it on the console and finally analyze the result.

let myAge = 20;
    console.log(typeof myAge); //number datatype

// end of Task 3

// Task 4 - I want to store a value that represents nothingness into a variable, let's say the variable name is totalUsers, what would be the preferred value to store there? 

    var totalUsers = null;
    console.log(totalUsers);
    
// end of Task 4

//Task 5 - what might be the solution in case if I wanted to store a phone number into  a variable, which data type would suit this scenario. 

let phoneNum = "+97798667777";
console.log(typeof phoneNum); //number datatype

// end of Task 5

//Task 6 - Scenario: I wanted to store data related to a specific object. For that create an object which would store some information related to the laptop you use.
let laptop = {
    brand : "lenovo",
    ram : "8gb",
    Os : "Windows10",
    message : function(){
        console.log("I use " + this.Os + " on my " + this.brand);
    }
}
laptop.message();
// end of Task 6

//Task 7: Create an object where the object keys store values of different data types and use typeof operator to check the data type of the value that you stored into it.
let person = {
    name : "xyz",
    age : 25,
    isStudent : true
}
console.log(typeof person.name);
console.log(typeof person.age);
console.log(typeof person.isStudent);

// end of Task 7

//Task 8: use typeof operator to check the value returned by the prompt() function

let value = prompt("Enter the value");
console.log(typeof value); //return value is a string

// end of Task 8

//Task 9: Create a constant that holds a value called red in BG_COLOR constant.

const BG_COLOR = "red";
console.log(BG_COLOR);

// end of Task 9

//Task 10: try to declare the constant without assigning the value to it and see the result, also try to re-assign the value to the same constant and see the result.


/** gives Uncaught SyntaxError: Missing initializer in const declaration if declared constatnt without assignin the value */
const personName = "Ram";
console.log(personName);

// end of Task 10

